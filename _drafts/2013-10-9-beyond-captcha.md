---
layout: post
date: 2013-10-09 12:00
title: Beyond Captcha
tags: [Marketers, Captcha, Jokes]

---

Captcha is a sort of puzzle that software applications use to try and determine whether or not you are a real person. 

Here are some methods you can use to take it a step further and try and determine whether or not a person is a marketer.

1. Does their bio explain how valuable they can be to you?
2. Do they actively use LinkedIN?
3. Do they post links to "White Pages"?
4. Do they talk about "social media"?
5. Does their profile photo look like a headshot?*

If you do some of these things, I have bad news for you: you are a marketer. Even if you consider yourself a "creative" or a "developer", if you use your time with people to try and sell yourself or your product, you are a marketer.

*Could also be an actor, stay away from those too.

