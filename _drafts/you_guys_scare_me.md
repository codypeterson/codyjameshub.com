#We scare me

I've had this vague idea for the last few years that when my generation takes the helm we won't go down the same rabbit holes as the generation before us. My generation is smarter, classier, and more ethical. We believe in human rights and work to protect them. We believe in open government, open companies (Balanced, Gittip), and open source. [ Expand on this a bit ]

What I am beginning to see though, is that not only will we go down the same rabbit holes, but we will go much further than anyone before us. We will use our technological prowess to make our spam seem ultra-realistic [give examples]. We will 