---
layout: post
date: 2013-06-26 12:00
title: Nebraska Bookended by Nebraska
tags: nebraska portland

---

In 1982 Bruce Springsteen released [*Nebraska*](http://www.youtube.com/watch?v=a2S2aM26QtI), a sparse album recorded alone in his New Jersey bedroom.

Later this year, in the fall of 2013, Alexander Payne will release his 6th film, titled [*Nebraska*](http://www.youtube.com/watch?v=kqIR22t5AI0).

My life started here in Nebraska just two years after Springsteen released that beautiful album and it's pretty likely that it will end here as well. For the foreseeable future though, I will be moving around and experiencing some new places.

This September I will be moving to [Portland, Oregon](http://www.youtube.com/watch?v=jZqB5R8afjw) to see what they have going on up there. I visited last year for the [XOXO Festival](http://xoxofest.com) and really enjoyed the city as well as the surrounding forests, beaches, and mountains. I'll be there at least a year. After my first lease is up I may go somewhere else (Charleston?, SF?, NYC?) or perhaps stay in Portland or come back to Nebraska.

My girlfriend [Ellen](http://ellenwilde.com) and our friends [Lindsay](http://lindsaytrapnell.com) and [Melissa](http://youthrevisited.us) will be moving to Portland as well and I am excited to have some exploration buddies.

I will continue working with my clients and collaborators in Nebraska and will be visiting every couple months to make sure they don't forget what my face looks like.

Thanks for the good times, Nebraska.