---
layout: post
date: 2012-06-02 18:55
title: Pledges from a Five Year Old
excerpt: A brief reflection on five years in the design field

---

It's been over fifteen years since I first dabbled in programming (making text adventure games with my brother Luke in QBasic), over twelve years since I created my first website (for a band my friends in high school were in), and five years since I decided to make this my career and landed my internship with SecretPenguin. 

In these last five years I have been very fortunate to be able to work with and learn from some of the most creative and talented people in Omaha.  Here is the shortlist of folks whom I consider my mentors: John Henry Muller of [What Cheer](http://whatcheer.com), Adam Nielsen of [GOODTWIN](http://good-twin.com), Matt Linder of [The New BLK](http://thenewblk.com), Alex Gates of [What Cheer](http://whatcheer.com), [Jerod Santo](http://jerodsanto.com) of RSDi and, of course, Dave Nelson of [SecretPenguin](http://secretpenguin.com). 

---

Over the next five years I pledge to continue learning (from these people and whoever else I can trick into teaching me things). I also pledge my dedication to the highest standards of professionalism, integrity, and competence in design practice and promise to uphold the spirit and letter of the [Code of Professional Conduct](http://designproacademy.org/code-of-professional-conduct.html) through consistent practice and habitual reflection on my actions as outlined by the [Academy of Design Professionals](http://designproacademy.org/). 
