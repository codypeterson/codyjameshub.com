---
layout: post
date: 2013-09-22 12:00
title: Don’t Copy That Finger
excerpt: Don’t trust consumer products with your sensitive information.
tags: privacy security Apple iOS

---

The [“Chaos Computer Club”](http://www.ccc.de/en/updates/2013/ccc-breaks-apple-touchid) has apparently broken Apples new TouchID featured on the new iPhone 5s (released just days ago). This will, for many, further cement the fact that fingerprint biometrics aren’t, and may never be, ready for the world.

This isn’t a problem for me. I don’t keep much information that anybody would want to steal on my phone and am not too worried about someone stealing my fingerprint and creating a “fake finger” to get access to it. For me, the TouchID should work fantastically and I look forward to not having to type in my four-digit code.

As a general rule of thumb*; if you are someone that stores valuable, confidential, or potentially incriminating information on your phone, you should never trust the security measures provided by consumer products.

*Sadly, the pun was intended.