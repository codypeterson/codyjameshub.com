---
layout: post
date: 2012-05-27 12:00
title: On Writing

---

I write a lot. I believe that, as Stefan Sagmesiter [so beautifully stated](http://www.youtube.com/watch?v=o8PkFSLuYOk), "Keeping a Diary Supports Personal Development". In the past few years it has been mostly in Evernote but has moved more recently to numerous [Field Notes](http://fieldnotesbrand.com/) notebooks. For the most part this writing does not see the light of day due to my lack of confidence. It is my hope that this blog will help increase my confidence and skill in writing. 

My friend [Chris Kollars](http://christopherkollars.com/), who is a super talented designer and front-end developer, has also just started a blog so I am hoping that we are able to motivate each other to post often and write good content. 

The main topics discussed on this blog will be design, technology, and business. I do, however, reserve the right to include essays on music, art, and literature and other interests of mine (basketball). 
