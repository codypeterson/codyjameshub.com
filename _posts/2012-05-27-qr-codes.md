---
layout: post
date: 2012-05-27 13:30
title: Let's Not Sabotage Ourselves
tags: QR Mobile
excerpt: The Future of QR Codes and How We Can Shape It

---

QR codes speed up the process of getting a user interacting with your content*. But QR technology is less than perfect. The uniform appearance coupled with being unreadable by humans not only make it an easy target for hijacking (simply place a sticker of a QR code pointing to your website on top of someone else's advertisement) but also psychologically ties your campaign to other QR campaigns a user has experienced. This can be bad news due to QR codes being so often misused or [poorly executed](http://wtfqrcodes.com/post/19191119013/its-a-whole-new-package-help-us-open-it).

A technology that is easily abused and often misused has an uncertain future. But if we all do our part in correctly using the technology, we can give our users quick, fun, and interesting experiences that text URLs cannot offer. 

*Here are some tips on how we can better use QR codes to provide our users with memorable and useful experiences:*

##Make It Worth It
If a user is going to take the time to download a QR code application, launch it, and hold up their phone to your QR code, make it worth it for them. Either reward them with a unique and interesting user experience; or perhaps give them a coupon, discount, or other reward. *Users will only continue scanning QR codes if we give them reasons to.*

![](http://codyjamespeterson.com/blog/images/cupcake.gif)

##Don't Be a Bully
The bigger the QR code on your campaign is, the more interesting the experience should be. If a giant QR code and the words **"SCAN THIS"** are the only content on your campaign, then you better be taking the user to Disney Land.

##Use the Buddy System
Remember that not everyone knows how, is able, or wants to use QR codes. Providing an alternate text link helps increase the likelihood that users will view your content. It also helps set expectations by allowing users to see the URL you are directing them at. 

![](http://codyjamespeterson.com/blog/images/buddySystem.gif)

##Set Expectations
As I mentioned above, it is important to set expectations. QR codes have unique characteristics that make them great for interactive experiences. If you are simply taking users to your static homepage, give them a heads-up with a little *"View Our Website"* note. This helps set users' expectations and not bum them out if they were expecting something more interesting. 

##Be Safe
QR hijacking isn't yet a widespread problem, *but it will be*. The more people adopt the technology, the more people will abuse it's ambiguity. Altering the link on a campaign doesn't sound too bad until you realize it could point to malicious code or unsavory content. The best way to avoid people covering your QR code with one of their own, is to integrate it into the graphics of the campaign. Obviously there are limitations on what you can do here but get creative. 

####Recap
QR codes will not be around forever. But while they are here we should try and use their strengths to craft better experiences for our users rather than trick them into viewing our brochureware websites.

Relevant Links: 

- [WTF QR CODES](http://wtfqrcodes.com/) - A blog dedicated to pointing out strange QR campaigns.
- [QRinkle](http://qrinkle.com/) - A great QR code tracking and reporting app.
- [HelloQR](http://helloqr.com/) - Another great QR code tracking and reporting app.
- [QR Codes Go to College (article)](http://www.archrival.com/ideas/13/qr-codes-go-to-college) - An article on QR code usage on college campuses.

*For the purpose of this article, I focused on QR codes used as links to web pages. It should be noted that QR codes do have other valuable uses.