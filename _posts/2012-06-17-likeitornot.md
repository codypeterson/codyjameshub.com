---
layout: post
date: 2012-06-17 23:10
title: Like It or Not
excerpt: Facebook's Flawed "Like" Button
tags: facebook
category: featured

---

## The Scene
Imagine this is the most offensive *blog post* of the 21st century, you pea-brained mumpsimus.

Since nobody but the author could ever *like* such a poorly written, hate-filled, piece of trash; this *blog post* will get **zero** *likes* via it's "Like button".

Over at Facebook.com someone will call the post out. Over 500 people will agree with the Facebook post and *like* it.

Afterward, when viewing this *blog post*, the "Like button" will have a count of 500. **FIVE HUNDRED PEOPLE** *like* this piece of trash *blog post*. What happened?

## The Flaw
Facebook tries to **aggregate** all the *likes* a web page gets. So it adds up all of the *likes* the actual button on the web page gets, as well as all the *likes*, *shares*, and *comments* any posts on Facebook.com about the web page get.

The problem is that Facebook doesn't know the sentiment of the posts on Facebook.com so it just **assumes it is all positive sentiment** and that everyone *likes* the web page.

This is actually a pretty big deal. It will make unpopular content seem much more popular than it really is: thus effecting subsequent viewers perception of the quality of the content.

I think Facebook is right in trying to aggregate the *likes* from both Facebook.com and the outside web page. An extremely popular web page could have thousands of people sharing and *liking* it on Facebook but only a few people clicking the button on the page. Having those *likes* fragmented would not do the web page justice. But there has to be a better way to do this.

## A Partial Fix
A partial fix could be to create an area on Facebook where you can see where all of the *likes* for a post came from and make it accessible from the "Like button". While people could still mistake an unpopular post for a popular one, they can dig deeper to find out what people actually think by viewing all of the Facebook posts to see the sentiment.  

Obviously, Facebook would need to adhere to user's privacy settings and not show posts unless the person viewing it has permission or it is a public post.

Updated June 18th, 2012
## A Full Fix
[@Tinyxl](http://twitter.com/tinyxl) pointed out that the issue is also a semantic one. I agree, though I can't see Facebook ever straying from "like".

Twitter's button avoids ambiguity by using the word "Tweet" which does not imply sentiment. I think this could work for Facebook. Their button for web pages could simply read "Facebook" and when a users clicks it they could choose to like, share, etc. This way the user can tell that the content is popular, unpopular, or not popular; but not imply that it was unanimously *liked* by those people.
