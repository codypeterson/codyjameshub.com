---
layout: post
date: 2012-10-30 11:45
title: Apps I Have Known
excerpt: The apps behind the shapes
tags: workflow productivity business

---

Today I worked on finishing up my third quarter tax return. While going through my expenses I evaluated the apps and services I have spent money on. While this did lead me to *cancel a few services*, it also made me think about the apps and services that I use that help streamline my workflow and business.

Here are *a few* of the apps that I use on a daily basis to either design and develop, or to streamline the others parts of business so I can do more of the creative stuff. 


#Desktop Apps
## Sublime Text 2
I switched from Coda to [Sublime Text](http://www.sublimetext.com/2) after I saw [Jerod Santo](http://objectlateral.com) using it during a meeting. Though I love Coda, ST2 can really sing while developing locally. 

##Codekit
[Codekit](http://incident57.com/codekit/) is pretty much my utility belt. It handles all of my LESS/SCSS compiling, minifying, javascript debugging, image optimization, and more. Alhough I have only been using it for 4 or 5 months, I can't imagine life without it. 

##Transmit
The old standby. [Transmit](http://panic.com/transmit/) is the most solid and beautifully designed FTP clients out there. 

##Github
Git has been the largest change to my workflow. The piece-of-mind that it provides takes a lot of stress out of my life and it makes collaborating a breeze. The [Github for Mac](http://mac.github.com/) application makes working with Github a cinch.

##Things
I recently switched from writing my to-dos on a plethora of notepads, blank sheets of paper, and post-it notes to using [Things](http://culturedcode.com/things/). 

##Mou
My preferred Markdown editor (and I have tried a handful). [Mou](http://mouapp.com/) touts itself as "The missing Markdown editor for web developers" but I use it mostly for writing essays or blog posts (like this one). I would recommend it for anyone who prefers writing with Markdown.

##Adobe CS6
You know the drill.


#Web Apps

##IFTTT
If it doesn't take creative thinking, automate it. [IFTTT](https://ifttt.com/wtf) is probably my favorite web service. Have it [text message you the weather in the morning](https://ifttt.com/recipes/search?utf8=%E2%9C%93&q=weather), or [post your blog posts to LinkedIN](https://ifttt.com/recipes/search?utf8=%E2%9C%93&q=linkedin), or notify you if the CDC reports a [Zombie outbreak](https://ifttt.com/recipes/search?utf8=%E2%9C%93&q=zombie).

##Harvest 
[Harvest](http://www.getharvest.com/) is how I make sure I am getting paid for the work I do. The time tracker is great and the main use for me (I love how it knows if a timer was left running), but I also use Harvest to send estimates, invoices, and reminders to clients.

##Basecamp
[Basecamp](http://basecamp.com/) is still one of the best ways to collaborate with clients and keep everyone accountable.

##Google Voice
My business number is powered by [Google Voice](https://www.google.com/voice). It's a super powerful tool and one of the many reasons that I love Google. If you call my business number and leave a message, there is a big chance that I am reading the horrible (but valuable) transcription before returning your call. 

##Webscript
[Webscript](https://www.webscript.io/) just launched today but I already love it. Today I set it up to replace my Pingdom account which will save me $5 a month. It's like IFTTT for developers. I can't wait to make it automate even more of my tasks.

##Dropmark
I mentioned [Dropmark](http://dropmark.com/) in my last blog post but wanted to mention it again. The best tool for moodboard collaboration. It has other interesting uses as well.

---

Note: The title of the post is referencing [this book](http://www.amazon.com/Casseroles-Have-Known-Flo-Price/dp/0876809530) that I once saw at a thrift store. 