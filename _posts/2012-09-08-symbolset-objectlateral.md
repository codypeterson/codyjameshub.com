---
layout: post
date: 2012-09-08 20:30
title: Object Lateral, Symbolset & Oak
tags: work symbolset dropmark objectlateral

---

My first post-agency project was the branding and website for a new software company, [Object Lateral](http://objectlateral.com). Run by my frequent collaborator, [Jerod Santo](http://jerodsanto.com), Object Lateral develops "thoughtful, custom software". If you are in need of app or software development, Object Lateral gets my recommendation.

##Links 
- [ObjectLateral.com](http://objectlateral.com)
- [My Object Lateral Dribbble shots](http://dribbble.com/codyjames/projects/69328-Object-Lateral)

The Object Lateral site marks my first use of [Symbolsets](http://symbolset.com/), which are "semantic symbol fonts" created by the fine folks at [Oak](http://oak.is/). I first became interested in the idea of symbol fonts back in January of 2010 after reading [this post](http://www.zachleat.com/web/css-sprites-using-font-face/) by the great [Zach Leatherman](http://twitter.com/zachleat) so it was nice to finally implement the idea.

Oak is also the creator of a wonderful service called [Dropmark](http://dropmark.com/) that I have been using over the last 6 months. I use it for mood-board creation but it has many uses. I will be keeping my eyes on this team.

##Links
- [Symbolset blog post mentioning Object Lateral](http://symbolset.com/blog/symbolset-loves-zaarly-stuff-and-nonsense-object-lateral/)
- [Oak](http://oak.is/)
- [Dropmark](http://dropmark.com/)

