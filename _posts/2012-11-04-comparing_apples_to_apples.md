---
layout: post
date: 2012-11-04 23:59
title: Comparing Apples to Apples
tags: business
excerpt: Why prices may differ from company to company
category: featured

---

In any profession it's hard for clients to understand why Company A charges more than Company B for the *same* product or service. Though there are a myriad reasons why prices are inconsistent between companies, the three main ones* are probably **Experience**, **Quality**, and **Accountability**.


#Experience
Having experience enables you to complete tasks quicker and with more efficiency, so why would this increase costs? It's simple, experienced professionals know all of the nuances of a project and can accurately estimate the time and costs involved. Those lacking experience *cannot be expected* to know everything involved in a specific task. This leads to lower time estimates which leads to a lower bid, extended timelines, and stress on both ends; *not a recipe for a good project.*


#Quality
There are two sides to quality: the quality that the client wants/expects, and the quality the company is willing/wanting to provide. Sometimes a client isn't looking for high-quality, and that can be okay, but not all companies *(present company included)* are willing to produce work that is below their company standards. It is **essential** that both parties are on the same page in regards to quality.


#Accountability
Accountability may not always directly affect price. But it's something to think about when choosing between Company A and Company B. If there are issues with the work, is this company willing *and able* to resolve them? Does this company have any sort of guarantee on their work?


#*Or*
Are experience, quality, and accountability the three ingredients for a successful project? Hardly.

Experience is nice, but it doesn't necessarily lead to greater outcomes. **Tenacity**, **Vision**, and **Taste** could easily trump experience in the right situations.

There is no substitute for quality. That being said, not all aspects of a project necessarily need to be of high "quality". For instance, sometimes the quality of a design could be much more important than the quality of materials used (or vice versa).

It may not be the case that the Company has the know-how or ability to provide support for the end product, and that can be okay, but they need to be **open** and **honest** about this so you know who to go to if an issue arises.

#Closing thoughts
All-in-all, weigh the importance of the project, the time-frame available, and the amount of resources you can dedicate to the project; once you take all of those into consideration, the choice should be clear.


*This post does not get into prices that may be inflated due to shady practices or other artificial factors, but beware that those exist in every industry.
