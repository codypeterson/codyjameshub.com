---
layout: post
date: 2012-06-25 10:30
title: Five Years with SecretPenguin
excerpt: or The Best Five Years of My Life

---

Starting July 1st, 2012, I will no longer be an employee of SecretPenguin. I will continue working with SecretPenguin on a few amazing projects currently in progress, but I will also be **available for consulting and freelance work**. I will be posting more about what I hope to accomplish in this new period of my life over the next few weeks but right now I want to talk about my time at SecretPenguin.

SecretPenguin is a special place to work. Dave Nelson holds a key quality that helps make a business special: **trust**. Early on, I decided I wanted to take on some of the front-end development, he trusted me. After a few years of doing design and front-end development I wanted to take on some of the back-end development. Not only did he trust me to make that shift, he also allowed me to **hire and train** my best friend, Jason (who had no previous experience with web), to take over the front-end development.

Over the last two years, as the team expanded, Dave trusted me with the roles of Art Director and Experience Director, and it has been a blast. We worked with some of the best clients a company could ask for and *created work that I am immensely proud of.* 

When my five year anniversary with SecretPenguin came up, I gave some thought into what I wanted to accomplish in the next five years of my career. I threw caution to the wind and decided that I wanted to experiment more, travel (hopefully), work with new people, and work on my own projects. 

When I told Dave what I was thinking, he told me to go for it. No hesitation. He trusts in me, and it feels great to have that from someone, especially Dave. 

Working with, and helping to manage, such a creative team is something that I truly enjoy, and when that is the *one thing* I want to be doing, somewhere like SecretPenguin is where I hope to find myself.

Here's to another great five years for SecretPenguin, and hopefully, for me. 

[Read Dave's Post](http://secretpenguin.com/2012/06/cody-moving-on/)