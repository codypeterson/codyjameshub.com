---
layout: post
date: 2013-08-29 12:00
title: Personal History Tours NET Interview
tags: [Personal History Tours, press]
category: featured

---

We were interviewed by Hilary Stohs-Krause for NET's series entitled "A Look At The Evolution Of An App From Idea To Product". The three-part series will follow the development of the project as well as some others doing similar things and talking about app development.

- Listen to part one: [http://netnebraska.org/article/news/look-evolution-app-idea-product](http://netnebraska.org/article/news/look-evolution-app-idea-product)
- Personal History Tours: [PersonalHistoryTours.com](http://PersonalHistoryTours.com)
