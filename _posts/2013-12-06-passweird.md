---
layout: post
date: 2013-12-06 10:12
title: Domain Squatting and Passweird
tags: passweird, micro-project, joke, dumb

---

Like a lot of people, I often get dumb ideas. And, like a lot of developers, I often buy domains for those ideas even if I am unlikely to actually act on them. I don't like this because **I hate domain squatters** and here I am owning a bunch of domains that I will never use.

This leads me to my newest idea: I want to create a domain parking service where people who own domains can offer them for free to people who are going to *actually use them*. This would allow us to keep good domains out of the hands of real domain-squatters (who just want to make money off of domains), and get them into the hands of people who will use them.

This is not something that I will work on anytime soon (if ever). In the meantime, if you have the need for any domain I own that I don't plan on using, **and are actually launching something**, you can have them, just let me know. I hope you will do the same with your domains.

Oh, and a few weeks ago I realized that a dumb domain I bought last year was about to expire. This was around the same time I was thinking a lot about domain squatters. Rather than just let it expire or keep squatting on it, I decided to make it a real, despite the fact that it *should not exist*. Introducing [Passweird](http://passweird.com), a website that generates passwords "too gross to steal". I contracted Omaha illustrator (and great dude), Matt Carlson [PlaidMtn.com](http://plaidmtn.com), to illustrate a bunch of super gross things, created a simple Sinatra app, and launched it.

I actually finished it a couple weeks ago but I am sort of embarrassed by it (it's real dumb). Now that this dumb joke is out of my system, I am going to continue working on things that are actually useful to people.