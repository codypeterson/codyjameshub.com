---
layout: post
date: 2012-07-31 22:15
title: Responsive Everything
excerpt: Size is just the beginning
tags: responsive accessibility semi-serious
category: featured

---

The current responsive design movement is quickly gaining momentum. Responding to device size is a powerful way to ensure device agnostic success on your projects. But we shouldn't stop there.

With iOS, we already have access to those devices' accelerometer & gyroscope, but I would love to see even more data available to developers. For instance, imagine having information about a user's accessibility needs. If you knew a user was color blind you could respond with a higher contrast design. Or, if you knew a user was hard of hearing, you could default your videos to closed captioned.

Now, get in there and use the accelerometer to abridge your content for users reading your blog while going for a run.
