---
layout: post
date: 2013-04-09 14:25
title: A Wad of Toilet Paper Jammed Into a Toilet
tags: etiquette collaboration

---

You walk into a single stall restroom and find that someone has left a horrible mess. What do you do? You can't leave it, someone may see you and blame the mess on you. You have to clean it up. It's the worst.

This happens with work too, you may not have created the mess, but sometimes you have to clean it up, otherwise, someone may see you leave before finding a giant wad of toilet paper jammed into the toilet.

Let's all agree to just not leave a mess in the restroom.

-----

You can also read this article on [Medium](https://medium.com/working-together/8d94df2d7eb4).
