---
layout: post
date: 2013-04-08 10:00
title: On Hourly Rates
tags: freelance consulting rates business

---

Being a freelancer is great. I get to set my own hours (I like to be wacky and do a 9am to 5pm thing), work on the projects I want to work on, and pick the team for each project. I also get to be asked, "How much is your hourly rate?" *all the time*.

If that is the first question you ask, we are starting off on the wrong foot. By asking someone’s hourly rate before anything else, it may seem like you are just looking for someone cheap to work with rather than **wanting to work with them**.

Here are a few of my thoughts on hourly rates and what they mean.

##First Things First
When looking for developers, illustrators, or designers to collaborate with, I first look for someone who does great work, then I make sure they are interested in the project and make sure we understand each other and can communicate well (via phone or in-person). After all of that, I ask them about their hourly rate, just to make sure it is in-line with budgets. If the budget isn't large enough to bring this person on but I believe they are the right person for the job, I try and reduce scope or find the money to bring them on. There is nothing better then feeling like you have *the right team for the job*.

##Higher Hourly Rates Don't (Necessarily) Equal Higher Project Costs
People don't work at the same speed. Someone with experience may be able to complete a task in half the time as someone with less experience, not to mention the potential difference in quality. To assume that higher hourly rates will lead to higher project costs is shortsighted. Quality work saves money. Period.

##Appropriate Hourly Rates Allow Freelancers to Focus
You don't want to work with people who have to work 80 hour weeks on a dozen projects just to make a decent living. Working with someone who can give your project the attention it deserves will produce better outcomes. 
