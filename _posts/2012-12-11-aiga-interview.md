---
layout: post
date: 2012-12-11 9:29
title: Interview with AIGA Nebraska
tags: press interview
category: featured

---

I was recently interviewed by Kristin DeKay (of Grain & Mortar) for AIGA Nebraska. The interview focuses on my transition to self-employement over the last 6 months. Doing the interview was fun and a great opportunity for me to think deeply about the last 6 months.

[Read the interview](http://nebraska.aiga.org/interview-cody-peterson/)
