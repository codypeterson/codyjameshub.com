---
layout: post
date: 2013-4-06 18:20
title: MindMixer & Baseball
tags: baseball MindMixer

---

Last week I started a two to three month contract with the great folks at [MindMixer](http://mindmixer.com). It will be four days a week so I won't be taking on much other work (but hope to work on some personal projects). I feel lucky that Nathan and Nick (Founders of MindMixer) wanted me to work on their amazing project. With their product team now including [Justin Kemerling](http://justinkemerling.com/), Jimmy Winter, Matt Barr, and others; MindMixer's future looks bright and I am glad to be part of it, if only for a little while. 

Oh, and the [weekly baseball pick-up game](http://www.facebook.com/pages/Old-Market-Industrial-Sunday-Pick-Up-Game/135029216514563) I play starts tomorrow. It's going to be a good spring.