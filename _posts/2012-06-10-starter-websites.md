---
layout: post
date: 2012-06-10 18:50
title: Starter Websites
tags: Content Strategy, Starter Websites
excerpt: Avoiding Monocarpic Endeavors

---

Not everyone can maintain a website or blog, in fact, very few people have this ability. Before sinking time and money into a beautiful website that *flowers once and then slowly dies*, I propose that you may be better served by first creating a Starter Website where you can practice maintaining and updating a website and learn how to drive people to it.

What does a Starter Website look like? Well, you are looking at one now. I designed and built this blog in a day by forcing myself to use only text and keep it really simple. Once I prove to myself that I can maintain and care for it, I will dedicate time and money to something that can more accurately express my personality and ideas. This will help ensure that I **do not waste resources** on something that just collects dust.

Design can help draw users in but content is what will keep them coming back. Working with a really good Content Strategist rather than a Designer may better serve you as you figure out how to grow and maintain a user/customer base.

Now go, create your Starter Website. Become one of the few people that have the ability to maintain a website and you will stand out from others, if only because you are *perennial*. Then, work with a smart design agency to create a website that pulls people in and sells whatever widget/idea/gum you are trying push.
