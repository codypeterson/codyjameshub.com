---
layout: post
date: 2013-11-04 14:30
title: Flywheel Interview
tags: [Interview]
category: featured

---

A few weeks ago I was interviewed for the [Flywheel](http://getflywheel.com) blog. Flywheel is a beautiful Wordpress hosting service. I used it on a couple recent sites and absolutely love it.

You can read the interview here: [http://getflywheel.com/designer-spotlight-cody-peterson/](http://getflywheel.com/designer-spotlight-cody-peterson/)
